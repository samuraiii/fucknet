<?php

class User
{

    public function __construct($db, $username)
    {
        $this->db = $db;
        $this->user_data = $this->db->getUserData($username);
        $this->username = $this->user_data['username'];
        $this->user_id = $this->user_data['id'];
        $this->haters = $this->getHaters();
        $this->hates = $this->getHates();
        // Array ( [0] => 27 [id] => 27
        // [1] => samurai [username] => samurai
        // [2] => konflic@mail.ru [email] => konflic@mail.ru
        // [3] => 7e6e0c3079a08c5cc6036789b57e951f65f82383913ba1a49ae992544f1b4b6e [password] => 7e6e0c3079a08c5cc6036789b57e951f65f82383913ba1a49ae992544f1b4b6e
        // [4] => 2018-11-05 00:00:00 [signup_date] => 2018-11-05 00:00:00
        // [5] => assets/images/profile_pic/default.png [profile_pic] => assets/images/profile_pic/default.png
        // [6] => 0 [num_posts] => 0 [7] => 0 [num_likes] => 0 [8] => 0 [closed] => 0 [9] => , [friends_array] => , )
    }

    public function getUserName()
    {
        return $this->username;
    }

    public function getUserLink()
    {
        return '<a href="/' . $this->getUserName() . '">' . $this->getUserName() . '</a>';
    }

    public function isAdmin()
    {
        $query = "SELECT id FROM admins WHERE user_id = ?";
        return $this->db->query($query, $this->user_id)->numRows();
    }

    public function myId($user_id)
    {
        return $this->user_id == $user_id;
    }

    public function myUsername($username)
    {
        return $this->getUserName() == $username;
    }

    public function getHatersNumber()
    {
        return count($this->haters);
    }

    public function getHatesNumber()
    {
        return count($this->hates);
    }

    public function getHaters()
    {
        $query = "SELECT haters FROM haters WHERE id = ?";
        $result = $this->db->query($query, $this->user_id)->fetchArray();
        return array_filter(explode(",", $result['haters']));
    }

    public function getHates()
    {
        $query = "SELECT hates FROM haters WHERE id = ?";
        $result = $this->db->query($query, $this->user_id)->fetchArray();
        return array_filter(explode(",", $result['hates']));
    }

    public function getUserIdByName($username)
    {
        $query = "SELECT id FROM users WHERE username = ?";
        $result = $this->db->query($query, $username)->fetchArray();
        return $result['id'];
    }

    public function getUserNameById($user_id)
    {
        $query = "SELECT username FROM users WHERE id = ?";
        $result = $this->db->query($query, $user_id)->fetchArray();
        return $result['username'];
    }

    public function getNumPosts()
    {
        return $this->user_data['num_posts'];
    }

    public function getProfilePic()
    {
        return $this->user_data['profile_pic'];
    }

    public function addOnePost()
    {
        $num_posts = $this->getNumPosts();
        $num_posts++;
        $query = "UPDATE users SET num_posts = ? WHERE username = ?";
        $this->db->query($query, $num_posts, $this->username);
    }

    public function isClosed()
    {
        if ($this->user_data['closed'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    public function iHate($user_id)
    {
        if ($this->myId($user_id)) {
            // Себя всегда ненавижу
            return true;
        }
        return in_array($user_id, $this->hates);
    }

    public function iAmHatedBy($user_id)
    {
        if ($this->myId($user_id)) {
            // Себя всегда ненавижу
            return true;
        }
        return in_array($user_id, $this->haters);
    }

    private function removeFromHaters($user_id)
    {
        if (!$this->iAmHatedBy($user_id)) {
            return true;
        }
        $haters = $this->haters;
        if (($key = array_search($user_id, $haters)) !== false) {
            unset($haters[$key]);
        }
        $haters = implode(",", $haters);
        $query = "UPDATE haters SET haters = ? WHERE id = ?";
        $this->db->query($query, $haters, $this->user_id);
        // Обновляем ненависти пользователя
        $this->haters = $this->getHaters();
    }

    private function removeFromHates($user_id)
    {
        if (!$this->iHate($user_id)) {
            return;
        }
        $hates = $this->hates;
        if (($key = array_search($user_id, $hates)) !== false) {
            unset($hates[$key]);
        }
        $hates = implode(",", $hates);
        $query_hates = "UPDATE haters SET hates = ? WHERE id = ?";
        $this->db->query($query_hates, $hates, $this->user_id);
        // Обновляем ненависти пользователя
        $this->hates = $this->getHates();
    }

    private function addToHates($user_id)
    {
        if (!$this->iHate($user_id)) {
            $hates = $this->getHates();
            array_push($hates, $user_id);
            $hates = implode(",", $hates);
            $query_hates = "UPDATE haters SET hates = '$hates' WHERE id = '$this->user_id'";
            $this->db->query($query_hates);
            // Обновляем ненависти пользователя
            $this->hates = $this->getHates();
        }
    }

    private function addToHaters($user_id)
    {
        if (!$this->iAmHatedBy($user_id)) {
            $haters = $this->haters;
            array_push($haters, $user_id);
            $haters = implode(",", $haters);
            $query = "UPDATE haters SET haters = '$haters' WHERE id = '$this->user_id'";
            $this->db->query($query);
            $this->haters = $this->getHaters();
        }
    }

    public function hate($user_id)
    {
        if ($user_id == $this->user_id) {
            // Защита от попыток добавить себя в ненависть
            return "";
        }
        // Ecли я ещё не ненавижу пользователя
        $user_link = '<a href="/' . $this->getUserName() . '">' . $this->getUserName() . "</a>";

        if (!$this->iHate($user_id)) {
            // Добавить в hates текущего пользователя
            $this->addToHates($user_id);
            // Добавить в haters целевого пользователя
            $target_user = new User($this->db, $this->getUserNameById($user_id));
            $target_user->addToHaters($this->user_id);
            $message = "User " . $user_link . " started hating you!";
            $target_user->setNotification($message, "hate");
            echo "Unhate";
        } else {
            // Удалить из hates моей ненависти
            $this->removeFromHates($user_id);
            // Удалить из haters пользователя
            $target_user = new User($this->db, $this->getUserNameById($user_id));
            $target_user->removeFromHaters($this->user_id);
            $message = "User " . $user_link . " not hating you any more!";
            $target_user->setNotification($message, "hate");
            echo "Hate";
        }
    }

    public function setNotification($text, $type)
    {
        // Выставление уведомления для пользователя
        $date = date("Y-m-d H:i:s");
        $notification_query = "INSERT INTO notifications (user_id, message, type, date_recieved) VALUES ('$this->user_id', '$text', '$type' ,'$date')";
        $this->db->query($notification_query);
    }

    public function getNotificationsList()
    {
        // Получение списка уведомлений для пользователя
        $get_notification = "SELECT id, user_id, message, date_recieved FROM notifications WHERE user_id = ? AND read_by_user = '0'";
        $notifications = $this->db->query($get_notification, $this->user_id)->fetchAll();
        return $notifications;
    }

    public function hasUnreadNotifications()
    {
        // Проверить что есть непрочитанные уведомления
        $get_notification = "SELECT id FROM notifications WHERE user_id = ? AND read_by_user = '0' LIMIT 1";
        return $this->db->query($get_notification, $this->user_id)->numRows();
    }

    public function renderHatedPosts()
    {
        // Вывожу посты которые хейтит пользователь
        $query = "SELECT post_id FROM post_hates WHERE user_id LIKE ?";
        $result = $this->db->query($query, $this->user_id)->fetchAll();
        foreach ($result as $post_id) {
            $post = new PostItem($this->db, $post_id['post_id'], $this);
            $post->renderHtml();
        }
    }

    public function readNotification($notification_id)
    {
        // Отметить уведомление как прочитанное
        $query = "UPDATE notifications SET read_by_user = '1' WHERE id = '$notification_id'";
        $this->db->query($query);
        header('Content-type: application/json');
        echo json_encode(array('status' => 'ok'));
    }
}
