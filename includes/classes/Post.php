<?php

class Post
{

    private $user_object;

    public function __construct($db, $user)
    {
        $this->db = $db;
        $this->user_object = new User($db, $user);
    }

    public function submitPost($body, $user_to)
    {
        $body = strip_tags($body);
        $body = htmlspecialchars($body);
        $check_empty = preg_replace('/\s+/', '', $body);

        // Check if post is not empty
        if ($check_empty != '' && strlen(trim($body)) < 20) {
            $date_added = date('Y-m-d H:i:s');
            $added_by = $this->user_object->getUsername();
            // If post to myself
            if ($user_to == $added_by) {
                $user_to = null;
            }
            // Insert post
            $query = "INSERT INTO posts VALUES (NULL, '$body', '$added_by', '$user_to', '$date_added', 'no', 'no', 0, 0, ',')";
            $this->db->query($query);

            // Insert notification
            // TODO

            // Update post count for user
            $this->user_object->addOnePost();
        } else {
            echo 'BOOOOOM';
        }
    }
}
