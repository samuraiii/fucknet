<?php

class PostItem
{
    /*
    Class for a single post Item
     */

    public function __construct($db, $post_id, User $current_user)
    {
        $this->db = $db;
        $this->post_id = $post_id;
        $this->post_data = $db->query("SELECT * FROM posts WHERE id = ?", $post_id)->fetchArray();
        $this->added_by = $this->post_data["added_by"];
        $this->user_to = $this->post_data["user_to"];
        $this->deleted = $this->post_data["deleted"];
        $this->num_comments = $this->post_data["num_comments"];
        $this->body = $this->post_data["body"];
        $this->current_user = $current_user;
        $this->date_added = $this->post_data["date_added"];
    }

    public function isDeleted()
    {
        return $this->deleted == "yes";
    }

    private function postAuthor()
    {
        return new User($this->db, $this->added_by);
    }

    public function deletePost()
    {
        $this->db->query("INSERT INTO posts (deleted) VALUES (?) WHERE id=?", "yse", $this->post_id);
    }

    public function commentsAmount()
    {
        $data = $this->db->query("SELECT num_comments FROM posts WHERE id = ?", $this->post_id)->fetchArray();
        return $data['num_comments'];
    }

    public function getPostHatesNumber()
    {
        $query = "SELECT COUNT(user_id) FROM post_hates WHERE post_id = ?";
        return $this->db->query($query, $this->post_id)->fetchArray()["COUNT(user_id)"];
    }

    public function addComment($comment_body)
    {
        // Добавить посту комментарий
        $date = date('Y-m-d H:i:s');
        $this->db->query("INSERT INTO post_comments (comment_body, posted_by, posted_to, removed, post_id, date_added) VALUES (?, ?, 'all', 'no', ?, ?)", $comment_body, $this->current_user->getUserName(), $this->post_id, $date);
        $this->db->query("UPDATE posts SET num_comments = num_comments + 1 WHERE id=?", $this->post_id);
        echo $this->commentsAmount();
    }

    public function addPostHate(User $user)
    {
        $query = "INSERT INTO post_hates (post_id, user_id) VALUES (? ,?)";
        $user_link = '<a href="/' . $user->getUserName() . '">' . $user->getUserName() . "</a>";
        $message = "User " . $user_link . " hates your post!";
        if ($this->postAuthor()->user_id != $user->user_id) {
            // Не добавлять уведомления для своих постов
            $this->postAuthor()->setNotification($message, "hate_post");
        }
        return $this->db->query($query, $this->post_id, $user->user_id);
    }

    public function removePostHate(User $user)
    {
        $query = "DELETE FROM post_hates WHERE post_id = ? AND user_id = ?";
        return $this->db->query($query, $this->post_id, $user->user_id);
    }

    public function isHatedBy(User $user)
    {
        $query = "SELECT post_id FROM post_hates WHERE post_id = ? AND user_id = ? LIMIT 1";
        return $this->db->query($query, $this->post_id, $user->user_id)->fetchAll();
    }

    public function hateToogle(User $user)
    {
        if ($this->isHatedBy($user)) {
            $this->removePostHate($user);
        } else {
            $this->addPostHate($user);
        }
        echo json_encode(['amount' => $this->getPostHatesNumber()]);
    }

    public function renderHtml()
    {
        $this_username = $this->current_user->getUsername();
        $added_by_obj = new User($this->db, $this->added_by);
        $added_by = $this->added_by;
        $user_to = $this->user_to;

        if ($this_username == $added_by) {
            // Собщения добавленные владельцем стены
            if ($user_to == 'none' || $user_to == $this_username) {
                if ($this_username == $_SESSION['user']) {
                    $added_by_username = "by <a href='/" . $added_by . "'>" . " me " . "</a>";
                    $user_to = '';
                } else {
                    // Если это стена не пользователя который залогинен
                    $added_by_username = "by <a href='/" . $added_by . "'>" . $added_by . "</a>";
                    $user_to = '';
                }
            } else {
                if ($this_username == $_SESSION['user']) {
                    $added_by_username = "from <a href='/" . $added_by . "'>" . " me " . "</a>";
                    $user_to = " to <a href='/" . $user_to . "'>" . $user_to . "</a>";
                } else {
                    $added_by_username = "from <a href='/" . $added_by . "'>" . $added_by . "</a>";
                    $user_to = " to <a href='/" . $user_to . "'>" . $user_to . "</a>";
                }
            }
        } else {
            // Собщения добавленные не владельцем стены
            if ($user_to == 'none' || $user_to == $added_by) {
                $added_by_username = "from <a href='/" . $added_by . "'>" . $added_by . "</a>";
                $user_to = '';
            } else {
                $added_by_username = "by <a href='/" . $added_by . "'>" . $added_by . "</a>";
                $user_to = " to <a href='/" . $user_to . "'>" . $user_to . "</a>";
            }
        }

        // Define user pictures
        $added_by_profile_pic = $added_by_obj->getProfilePic();
        $auth = new User($this->db, $_SESSION['user']);
        $auth_user_profile_pic = $auth->getProfilePic();

        // define hate text
        if ($this->isHatedBy($this->current_user)) {
            $hate_status = 'unhate';
        } else {
            $hate_status = 'hate!';
        }

        // Timeframe
        $date_time_now = date("Y-m-d H:i:s");
        $start_date = new DateTime($this->date_added);
        $end_date = new DateTime($date_time_now);
        $interval = $start_date->diff($end_date);
        if ($interval->d < 1) {
            $time_message = "< 24 hrs. ago";
        } else {
            $time_message = $interval->days . " days ago";
        }

        $post = $this;
        include $_SERVER['DOCUMENT_ROOT'] . '/templates/blocks/wall_post.php';
    }

    public function renderComments()
    {
        $query = "SELECT * FROM post_comments WHERE removed='no' AND post_id=? ORDER BY id DESC";
        $result = $this->db->query($query, $this->post_id)->fetchAll();

        foreach ($result as $row) {
            $posted_by = $row['posted_by'];
            $comment_body = $row['comment_body'];
            $comment_date = $row['date_added'];
            $posted_by_obj = new User($this->db, $posted_by);
            $posted_by_profile_pic = $posted_by_obj->getProfilePic();

            // Timeframe
            $date_time_now = date("Y-m-d H:i:s");
            $start_date = new DateTime($comment_date);
            $end_date = new DateTime($date_time_now);
            $interval = $start_date->diff($end_date);
            if ($interval->d < 1) {
                $time_message = "< 24 hrs. ago";
            } else {
                $time_message = $interval->d . " days ago";
            }

            require $_SERVER['DOCUMENT_ROOT'] . "/templates/blocks/post_comment.php";
        }
    }
}
