<?php

class Wall
{

    public function __construct($db, User $user)
    {
        $this->db = $db;
        $this->user = $user;
    }

    public function getPostHaters($post_id)
    {
        return count($this->db->query("SELECT COUNT(user_id) FROM post_hates WHERE post_id = ?", $post_id)->fetchAll());
    }

    public function isHatedBy($post_id)
    {
        return $this->getPostHaters($post_id);
    }

    public function submitPost($body, User $user_from)
    {
        $body = htmlspecialchars(trim($body));
        $date_added = date('Y-m-d H:i:s');
        $user_to = $this->user;

        if (strlen($body) >= 20) {
            if ($user_from->getUserName() == $user_to->getUserName()) {
                // Пост на своей стене
                $user_to = $added_by = $user_from;
            } else {
                // Пост на чужой стене
                $added_by = $user_from;
                $user_to->setNotification("You have new post from " . $user_from->getUserLink() . "<br><i>" . $body . "</i>", "new_post");
            }
            $query = "INSERT INTO posts (body, added_by, user_to, date_added, user_closed, deleted, num_comments) VALUES (?, ?, ?, ?, 'no', 'no', 0)";
            $res = $this->db->query($query, $body, $added_by->getUserName(), $user_to->getUserName(), $date_added)->lastInsertID();
            $this->user->addOnePost();
            return $res;
        } else {
            return "to_short_post";
        }
    }

    public function loadPosts($data, $limit)
    {
        $page = $data['page'];
        $this_username = $this->user->username;

        if ($page == 1) {
            $start = 0;
        } else {
            $start = ($page - 1) * $limit;
        }

        $str = "";
        $query = "SELECT id, added_by FROM posts WHERE deleted='no' AND (added_by=? OR user_to=?) ORDER BY id DESC";
        $result = $this->db->query($query, $this_username, $this_username)->fetchAll();

        if (count($result) > 0) {

            $count_hidden_msgs = 0;
            $num_iterations = 0;
            $count = 1;

            foreach ($result as $row) {

                $post_id = $row['id'];
                $added_by = $row['added_by'];
                $added_by_id = $this->user->getUserIdByName($added_by);

                // I see msgs only from haters
                if (!$this->user->iAmHatedBy($added_by_id)) {
                    $count_hidden_msgs++;
                    continue;
                }

                // Check if closed
                $added_by_obj = new User($this->db, $added_by);
                if ($added_by_obj->isClosed()) {
                    continue;
                }

                $num_iterations++;
                if ($num_iterations < $start) {
                    continue;
                }

                // Once 10 posts loaded, break
                if ($count > $limit) {
                    break;
                } else {
                    $count++;
                }

                $post = new PostItem($this->db, $post_id, $this->user);
                $post->renderHtml();
            }

            if ($count > $limit) {
                $str .= "<input type='hidden' class='nextPage' value='" . ($page + 1) . "'>
                         <input type='hidden' class='noMorePosts' value='false'>";
            } else {
                $str .= "<input type='hidden' class='noMorePosts' value='true'>";
                $str .= "<p style='color: ivory;text-align: center;margin-bottom: 0px;'>Hidden: $count_hidden_msgs msg. The end.</p>";
            }
        } else {
            $str .= "<p style='color: ivory; text-align: center;'> No posts yet, looser</p>";
        } // if
        echo $str;
    }
}
