<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/db.php';
$success = false;

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password_repeat'])) {

    $captcha = $_POST['g-recaptcha-response'];

    // Верифицируем капчу
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array('secret' => constant('SECRET'), 'response' => $captcha);

    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($data),
        ),
    );

    $context = stream_context_create($options);
    $result = json_decode(file_get_contents($url, false, $context), true);

    if ($result["success"]) {
        $error_array = array();

        $username = htmlspecialchars($_POST['username']);
        $email = $username . "@fcknet.com";
        $password = $_POST['password'];
        $password_repeat = $_POST['password_repeat'];

        $username = str_replace(" ", "_", trim($username));
        $username = strtolower($username);

        $date = date("Y-m-d");

        if (!preg_match('/^\p{Latin}+$/', $username)) {
            $error_message = "Only latin letters allowed!";
        }

        if (strlen($username) > 15 || strlen($username) < 3 && !$error_message) {
            $error_message = "from 3 to 15 username, faggot";
        } else {
            $query = "SELECT username FROM users WHERE username = ?";
            $username_check = $db->query($query, $username)->numRows();

            if ($username_check > 0) {
                $error_message = "username taken, looser";
            }
        }

        if ($password != $password_repeat) {
            $error_message = "equal passwords, asshole!";
        }

        if (strlen($password) > 30 || strlen($password_repeat) < 5) {
            $error_message = "from 5 to 30 password, sucker!";
        }

        if (!$error_message) {
            // Encrypting password
            $password = hash('sha256', $password);

            // Profile picture
            $profile_pic = "assets/images/profile_pic/default.png";

            $query_insert = "INSERT INTO users (username, email, password, signup_date, profile_pic, num_posts, num_likes, closed) VALUES ('$username', '$email', '$password', '$date', '$profile_pic', 0, 0, 0)";
            $res_query = $db->query($query_insert);

            if (mysqli_connect_errno()) {
                $error_message = "Register error!";
            } else {
                $success = true;
                $user_id = $db->getUserId($username);
                $query_create_hates = "INSERT INTO haters (id, haters, hates) VALUES (?, '', '')";
                $res_query_haters = $db->query($query_create_hates, $user_id);
                unset($_POST);
            }
        }
    }
}
