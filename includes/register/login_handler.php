<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/db.php';

if (isset($_POST["login_password"]) && isset($_POST["login_user"])) {

    $captcha = $_POST['g-recaptcha-response'];

    // Верифицируем капчу
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array('secret' => constant('SECRET'), 'response' => $captcha);

    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($data),
        ),
    );

    $context = stream_context_create($options);
    $result = json_decode(file_get_contents($url, false, $context), true);

    if ($result["success"]) {

        $username = htmlspecialchars($_POST['login_user']);
        $login_password = $_POST['login_password'];
        $login_password = hash('sha256', $login_password);
        $query = "SELECT * FROM users WHERE username = ? AND password = ?";
        $username_check = $db->query($query, $username, $login_password)->numRows();

        if ($username_check == 1) {
            $_SESSION['user'] = $username;
            header('Location: index.php');
        } else {
            $error_message = "authorization error";
        }
    } else {
        $error_message = "authorization error";
    }
}
