<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/helpers.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/autoload.php';
require $_SERVER['DOCUMENT_ROOT'] . '/includes/db.php';

if (!isset($_SESSION['user'])) {
    header('Location: logout.php');
}

$db = get_my_db();
$user = new User($db, $_SESSION['user']);

if (!array_key_exists('profile_username', $_GET)) {
    $myProfile = true;
}

?>

<!DOCTYPE html>
<html lang="en">

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/templates/head.php';?>

<body>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/templates/site_header.php';?>