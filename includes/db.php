<?php

require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
require 'classes/DB.php';

function get_my_db()
{
    static $db;

    if (!$db) {
        $db = new DB(mysqli_connect(constant('DB_HOST'), constant('DB_USER'), constant('DB_PASSWORD'), constant('DB_NAME')));
        if (mysqli_connect_errno()) {
            echo "Connecction failed: " . mysqli_connect_errno();
        }
    }
    return $db;
}

$db = get_my_db();
