<?php

function cropImage($image_path, $file_extension) {
    list($width, $height) = getimagesize($image_path);
    $thumb = imagecreatetruecolor(300, 300);

    if ($file_extension == 'jpeg' || $file_extension == 'jpg') {
        $source = imagecreatefromjpeg($image_path);
    } else if ($file_extension == 'png') {
        $source = imagecreatefrompng($image_path);
    } else if ($file_extension == 'gif') {
        $source = imagecreatefromgif($image_path);
    }

    imagecopyresized($thumb, $source, 0, 0, 0, 0, 300, 300, $width, $height);
    imagepng($thumb, $image_path);
    imagedestroy($source);
    imagedestroy($thumb);
}