<?php

function loader($className) {
    $path = $_SERVER['DOCUMENT_ROOT'] . "/includes/classes/";
    $fullPath = $path . $className . ".php";
    include $fullPath;
}

spl_autoload_register('loader');
