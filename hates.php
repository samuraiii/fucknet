<?php

include $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';

$visitedProfile = $user;
$_GET['profile_username'] = $_SESSION['user'];

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require 'includes/blocks/user_block.php'; ?>
        <?php require 'includes/blocks/user_menu.php'; ?>

    </div>

    <div class="main">
        <div class="main_column column">
            <!-- main_column -->
            <h2>I hate them</h2>
            <div class="search-results">
                <?php
                $res = "";
                $user_hates = $user->getHates();
                foreach ($user_hates as $user_id) {
                    $result = new User($db, $user->getUserNameById($user_id));
                    require $_SERVER['DOCUMENT_ROOT'] . '/templates/blocks/search_result.php';
                }
                echo $res;
                ?>
            </div>
        </div>
    </div>

</div>