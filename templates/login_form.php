<div class="login_form">
    <form action="" method="post" id="login_form">
        <input type="text" name="login_user" placeholder="username" required/>
        <input type="password" name="login_password" placeholder="pswd" required/><br>
        <button
            class="g-recaptcha button"
            data-sitekey="6LfQfKQZAAAAAIgCKmv1udC7H9JTzd_naZWwqGOp"
            data-callback='onSubmitLogin'
            data-action='submit'
            type="submit"
            name="login_button">Login</button>
        <?php if ($error_message): ?>
            <div class="error"><?=$error_message;?></div>
        <?php endif;?>
        <a href="?register" id="register" class="register_link">registration</a>
    </form>
</div>
