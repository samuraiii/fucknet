<head>
    <meta charset="UTF-8">
    <meta author="Mikhail C.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (strpos($_SERVER['REQUEST_URI'], '/routes/login/') !== false): ?>
        <title>!Welcome</title>
        <link rel="stylesheet" type="text/css" href="/assets/css/register_sm.css">
        <script src="https://www.google.com/recaptcha/api.js"></script>
    <?php else: ?>
        <title>FckNet.</title>
        <script src="/assets/scripts/jquery.js"></script>
        <script src="/assets/scripts/bootstrap.js"></script>
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <link rel="stylesheet" href="/assets/css/font-awesome.css">
    <?php endif;?>
    <script>history.scrollRestoration = "manual";</script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(65378617, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
    });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/65378617" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <meta name="theme-color" content="#000000">
</head>