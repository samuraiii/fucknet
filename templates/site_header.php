<div class="header">
    <div class="logo">
        <h1><a href="/">FckNet.</a></h1>
    </div>
    <nav>
        <a class="home-mobile" href="/">
            <i class="fa fa-lg fa-home" aria-hidden="true"></i>
        </a>
        <a href="/routes/notifications/">
            <i class="fa fa-lg fa-bell-o <?php if ($user->hasUnreadNotifications()) echo "badgefa"; ?>" aria-hidden="true"></i>
        </a>
        <a href="/routes/search/">
            <i class="fa fa-lg fa-search" aria-hidden="true"></i>
        </a>
        <a href="/routes/hateposts/">
            <i class="fa fa-lg fa-bookmark" aria-hidden="true"></i>
        </a>
        <a href="/routes/settings/">
            <i class="fa fa-lg fa-cog" aria-hidden="true"></i>
        </a>
        <a href="/logout.php">
            <i class="fa fa-lg fa-sign-out" aria-hidden="true"></i>
        </a>
    </nav>
</div>