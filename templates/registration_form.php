<div class="register_form">
    <form action="?register" method="post" id="register_form">
        <input autocomplete="off" type="text" name="username" placeholder="username" required>
        <input autocomplete="off" type="password" name="password" placeholder="password" required>
        <input autocomplete="off" type="password" name="password_repeat" placeholder="confirm password" required><br>
        <button class="g-recaptcha button"
            data-sitekey="6LfQfKQZAAAAAIgCKmv1udC7H9JTzd_naZWwqGOp"
            data-callback='onSubmitRegister'
            data-action='submit'
            type="submit"
            name="register_button">Create Account</button>
        <?php if ($error_message): ?>
            <div class="error"><?=$error_message;?></div>
        <?php elseif ($success): ?>
            <div class="success">All good, <a href="/routes/login/">log in!</a></div>
        <?php endif;?>
        <a href="/" id="login" class="login_link">go to login</a>
    </form>
</div>