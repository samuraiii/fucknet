<div class="notification">
    <div class="notification_info">
        <div class="notification_text"><?=$notification["message"];?></div>
        <div class="notification_date"><?=$notification["date_recieved"];?> UTC</div>
    </div>
    <button class="btn btn-link" id="read" name="<?=$notification["id"];?>">Hide</button>
</div>