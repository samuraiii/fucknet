<div class='status_post'>
    <div class='post_info'>
        <div class='post_info_user_by_pic'><img src='/<?=$added_by_profile_pic;?>' width='40'></div>
        <div class='post_info_data'>
            <div class='post_info_by'><?=$added_by_username . " " . $user_to;?></div>
            <div class='post_info_time'><?=$time_message;?></div>
        </div>
    </div>
    <div class='post_body'><?=$post->body;?></div>
    <div class='post_data_section'>
        <a id='<?=$post->post_id;?>' href='#' class='post_comments_link'>comments</a><span class='comm_count'> <?=$post->num_comments;?></span>
        <a id='<?=$post->post_id;?>' href='#' class='post_hates'><?=$hate_status;?></a><span class='hates_count'> <?=$post->getPostHatesNumber();?></span>
    </div>
    <div class='post_comments_form'>
        <div class='post_comments_list'></div>
        <div class='post_comments_controls'>
            <img class="post_comments_controls_img" src='/<?=$auth_user_profile_pic;?>' width='38' height='38'/>
            <input maxlength='255' type='textarea' name='post_comment_body' placeholder="comment..." class='post_comments_controls_input'>
            <button class='post_comments_controls button' type='submit' name='send_comment' disabled></button>
        </div>
    </div>
</div>