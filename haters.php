<?php

include $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';

$visitedProfile = $user;
$_GET['profile_username'] = $_SESSION['user'];

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require 'includes/blocks/user_block.php'; ?>
        <?php require 'includes/blocks/user_menu.php'; ?>

    </div>

    <div class="main">
        <div class="main_column column">
            <!-- main_column -->
            <h2>They hate me</h2>
            <div class="search-results">
                <?php
                $res = "";
                $user_haters = $user->getHaters();
                foreach ($user_haters as $user_id) {
                    $result = new User($db, $user->getUserNameById($user_id));
                    require $_SERVER['DOCUMENT_ROOT'] . '/templates/blocks/search_result.php';
                }
                ?>
            </div>
        </div>
    </div>

</div>