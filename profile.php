<?php

require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';

if (isset($_GET['profile_username']) && !$db->userExists($_GET['profile_username'])) {
    header("Location: /404.php");
}

$myProfile = array_key_exists('profile_username', $_GET) && $_SESSION['user'] == $_GET['profile_username'];

if (!$myProfile) {
    $visitedProfile = new User($db, $_GET['profile_username']);
    setcookie("userVisied", $_GET['profile_username']);
} else {
    $visitedProfile = $user;
    setcookie("userVisied", $_SESSION['user']);
}

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_block.php';?>
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_menu.php';?>

    </div>

    <div class="main">
        <!-- main -->
        <div class="main_column column">
            <!-- main_column -->
            <?php if ($myProfile): ?>
                <h2>my wall</h2>
            <?php else: ?>
                <h2>the wall of <?=$visitedProfile->getUserName();?></h2>
            <?php endif;?>

            <?php if ($user->iHate($visitedProfile->user_id)): ?>
                <form class="post_form" action="profile.php" method="post">
                    <div class="form-group">
                        <textarea style="resize: none;" type="textarea" name="post_text" placeholder="post stuff here... 20 minimum" id="post_text"></textarea>
                    </div>
                    <input disabled class="post_btn button" type="submit" name="post" id="post_button" value="Post" />
                </form>
            <?php else: ?>
                <p class="notice">Hate <?=$visitedProfile->getUserName();?> to post here</p>
            <?php endif;?>

            <div class="posts_area"></div>
            <div id="go_back">UP</div>
            <img id='loading' src="assets/icons/loader.gif">

        </div><!-- main_column -->
    </div><!-- main -->
</div><!-- wrapper -->

</body>

<script type="text/javascript" src="assets/scripts/bundle.js"></script>
<script type="text/javascript" src="/assets/scripts/postmainloader.js"></script>

<script>
    $(".posts_area").on('input', function (e) {
        // If value len in comment body input is > 15 enable button
        var comment_length = $(e.target).val();
        if (comment_length.length > 15) {
            $('[name="send_comment"]').prop('disabled', false);
        } else {
            $('[name="send_comment"]').prop('disabled', true);
        }
});
</script>
</html>