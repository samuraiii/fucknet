$('#go_back').hide();

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const userLoggedIn = getCookie('userLoggedIn');
const userVisied = getCookie('userVisied');

// Control post text area input
$("#post_text").on('input', function (e) {
    // If value len in comment body input is > 20 enable button
    var post_text = $(e.target).val();

    if (post_text.length > 20) {
        $('#post_button').prop('disabled', false);
    } else {
        $('#post_button').prop('disabled', true);
    }
});

$(".post_form").click(function (e) {
    // Submit post on the wall
    e.preventDefault();

    request = "";
    if (e.target.id == "post_button" && request != "none") {
        request = "none";
        $("#post_button").prop('disabled', true);
        var post_text = $("#post_text").val();
        data = { 'action': 'submit_post', 'post_text': post_text, 'userLoggedIn': userLoggedIn, 'userVisied': userVisied }
        $.ajax({
            url: "/ajax/posts.php",
            type: "POST",
            data: data,
            cache: false,
            success: function (response) {
                // clear input, disable button, upd comments count
                $("#post_text").val("");
                $("#post_button").prop('disabled', false);
                $(".posts_area").prepend(response);
                request = '';
                return false;
            }
        });

    }
});

$('#go_back').each(function () {
    $(this).click(function () {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false;
    });
});

$(".posts_area").on("click", function (e) {
    // Load comments for post
    if (e.target.className == "post_comments_link") {

        var post_id = e.target.id;
        var request = "";
        $(e.target.parentElement.nextElementSibling).toggle();

        e.preventDefault(); // to prevent auto scroll to top

        if (request !== 'none') {
            data = { 'action': 'load_comments', 'get_comments_for': post_id, 'userLoggedIn': userLoggedIn }
            $.ajax({
                url: "/ajax/comments.php",
                type: "POST",
                data: data,
                cache: false,
                success: function (response) {
                    $(e.target.parentElement.nextElementSibling).find('.post_comments_list').html(response);
                    request = 'none';
                    return false;
                }
            });
        }
    }
    // Adding comment to post
    if (e.target.name == "send_comment") {
        let send_button = e.target;
        let status_post = send_button.parentElement.parentElement.parentElement;
        let com_count = $(status_post).find('.comm_count');
        let post_id = $(status_post).find('.post_comments_link').attr('id');
        let post_comment_body = $(status_post).find('[name="post_comment_body"]');
        let body = post_comment_body.val();
        let request = "";

        e.preventDefault(); // to prevent auto scroll to top

        if (!String.prototype.trim) {
            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, '');
            };
        }

        if (request !== 'none' && body.trim() !== '') {
            request = 'none';
            data = { 'action': 'add_comment', 'post_id': post_id, 'comment_body': body, 'userLoggedIn': userLoggedIn }
            $.ajax({
                url: "/ajax/comments.php",
                type: "POST",
                data: data,
                cache: false,
                success: function (d) {
                    // clear input, disable button, upd comments count
                    post_comment_body.val("");
                    $(send_button).prop('disabled', true);
                    com_count[0].innerText = " " + d;
                    request = '';
                    return false;
                }
            });
        }
    }
    // Hate and unhate post
    if (e.target.className == "post_hates") {
        var post_id = e.target.id;
        var cur_status = e.target.innerText;
        var request = "";

        e.preventDefault(); // to prevent auto scroll to top

        if (request !== 'none') {
            request = 'none';
            data = { 'post_id': post_id, 'userLoggedIn': userLoggedIn }
            $.ajax({
                url: "/ajax/hates.php",
                type: "POST",
                data: data,
                cache: false,
                success: function (data) {
                    if (cur_status == 'hate!') {
                        e.target.innerText = 'unhate';
                    } else {
                        e.target.innerText = 'hate!';
                    }
                    e.target.nextElementSibling.innerText = " " + JSON.parse(data)['amount'];
                    request = "";
                    return false;
                }
            });
        }
    }
});

$(".hated_posts_area").click(function (e) {

    // Load comments
    if (e.target.className == "post_comments_link") {

        var post_id = e.target.id;
        var request = "";
        $(e.target.parentElement.nextElementSibling).toggle();

        e.preventDefault(); // to prevent auto scroll to top

        if (request !== 'none') {
            data = { 'action': 'load_comments', 'get_comments_for': post_id, 'userLoggedIn': userLoggedIn }
            $.ajax({
                url: "/ajax/comments.php",
                type: "POST",
                data: data,
                cache: false,
                success: function (response) {
                    $(e.target.parentElement.nextElementSibling).find('.post_comments_list').html(response);
                    request = 'none';
                    return false;
                }
            });
        }
    }
    // Adding comment to post
    if (e.target.name == "send_comment") {
        let send_button = e.target;
        let status_post = send_button.parentElement.parentElement.parentElement;
        let com_count = $(status_post).find('.comm_count');
        let post_id = $(status_post).find('.post_comments_link').attr('id');
        let post_comment_body = $(status_post).find('[name="post_comment_body"]');
        let body = post_comment_body.val();
        let request = "";

        e.preventDefault(); // to prevent auto scroll to top

        if (!String.prototype.trim) {
            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, '');
            };
        }

        if (request !== 'none' && body.trim() !== '') {
            request = 'none';
            data = { 'action': 'add_comment', 'post_id': post_id, 'comment_body': body, 'userLoggedIn': userLoggedIn }
            $.ajax({
                url: "/ajax/comments.php",
                type: "POST",
                data: data,
                cache: false,
                success: function (d) {
                    // clear input, disable button, upd comments count
                    post_comment_body.val("");
                    $(send_button).prop('disabled', true);
                    com_count[0].innerText = " " + d;
                    request = '';
                    return false;
                }
            });
        }
    }
    // Hate and unhate post in hateposts of user
    if (e.target.className == "post_hates") {
        let post_id = e.target.id;
        let cur_status = e.target.innerText;
        let request = "";

        e.preventDefault(); // to prevent auto scroll to top

        if (request !== 'none') {
            request = 'none';
            data = { 'post_id': post_id, 'userLoggedIn': userLoggedIn }
            $.ajax({
                url: "/ajax/hates.php",
                type: "POST",
                data: data,
                cache: false,
                success: function (data) {
                    e.target.parentElement.parentElement.remove() // Удаляю пост
                    request = "";
                    return false;
                }
            });
        }
    }
});

$(".user_details_info").click(function (e) {

    // Hate and unhate user
    if (e.target.className == 'hate_user') {
        var user_to_hate = e.target.name;
        var user_hater = userLoggedIn;
        var request;

        if (request !== 'none') {
            request = 'none';
            data = { 'user_to_hate': user_to_hate, 'user_hater': user_hater, 'userLoggedIn': userLoggedIn }

            $.ajax({
                url: "/ajax/hates.php",
                type: "POST",
                data: data,
                cache: false,
                success: function (d) {
                    if (cur_status == 'Hate') {
                        e.target.innerText = 'Unhate';
                    } else {
                        e.target.innerText = 'Hate';
                    }
                    e.target.nextElementSibling.innerText = " " + d;
                    request = '';
                    return false;
                }
            });
        }
    }
});

$("#find").click(function (e) {

    var request = "";
    var search_request = $("#search").val();

    if (search_request.length < 2) {
        $(".search-results").html("<span style='color: ivory;'>Минимальный запрос 2 символа!</span>");
    }

    if (request !== 'none' && search_request.length >= 2) {
        $("#search").val("");

        $.ajax({
            url: "/ajax/search.php",
            type: "POST",
            data: "search_request=" + search_request,
            cache: false,
            success: function (response) {
                $(".search-results").html(response);
                request = 'none'; // Prevent duplicating requests
                return false;
            }
        });
    }
});

$(".notifications").click(function (e) {
    if (e.target.id == "read") {
        $.ajax({
            url: "/ajax/notifications.php",
            type: "POST",
            data: "user=" + userLoggedIn + "&id=" + e.target.name,
            success: function (data) {
                if (data["status"] == "ok") {
                    e.target.parentElement.remove();
                }
            }
        });
    }
});
