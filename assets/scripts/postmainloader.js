$(document).ready(function () {

    $('#loading').show();

    // Request for loading posts
    data = { 'action': 'load_posts', 'page': '1', 'userLoggedIn': userLoggedIn, 'userVisied': userVisied }
    $.ajax({
        url: "/ajax/posts.php",
        type: "POST",
        data: data,
        cache: false,
        success: function (response) {
            $('#loading').hide();
            $('.posts_area').html(response);
        }
    });

    $(window).scroll(function () {
        var request;

        if (request !== 'none') {

            var scroll_top = $(this).scrollTop();
            var page = $('.posts_area').find('.nextPage').val();
            var noMorePosts = $('.posts_area').find('.noMorePosts').val();

            if (scroll_top > 300) {
                $('#go_back').show();
            } else {
                $('#go_back').hide();
            }

            if ((document.body.scrollHeight == scroll_top + window.innerHeight) && noMorePosts == 'false') {
                $('#loading').show();
                data = { 'action': 'load_posts', 'page': page, 'userLoggedIn': userLoggedIn, 'userVisied': userVisied }
                request = $.ajax({
                    url: "/ajax/posts.php",
                    type: "POST",
                    data: data,
                    cache: true,
                    success: function (response) {
                        $('.posts_area').find('.nextPage').remove();
                        $('.posts_area').find('.noMorePosts').remove();
                        $('.posts_area').append(response);
                        $('#loading').hide();
                        request = 'none'; // Prevent duplicating requests
                        return false;
                    }
                });
            } // End if qjax
        } // if request
    }); //$(window).scroll

});
