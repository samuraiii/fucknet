register_link = document.getElementById('register');
login_form = document.querySelector("div.login_form");

login_link = document.getElementById('login');
register_form = document.querySelector("div.register_form");

register_form.style.display = "none";

register_link.addEventListener('click', function(){
	register_form.style.display = "block";
	login_form.style.display = "none";
});

login_link.addEventListener('click', function(){
	register_form.style.display = "none";
	login_form.style.display = "block";
});

function onSubmitLogin(token) {
	document.getElementById("login_form").submit();
};

function onSubmitRegister(token) {
	document.getElementById("register_form").submit();
};