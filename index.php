<?php

session_start();

header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if (isset($_SESSION['user']) && !isset($_POST['post'])) {
    header("Location: " . $_SESSION['user']);
    setcookie("userLoggedIn", $_SESSION['user']);
} else if (!isset($_SESSION['user'])) {
    header("Location: logout.php");
}
