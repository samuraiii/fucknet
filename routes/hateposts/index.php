<?php

require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';

$visitedProfile = $user;
$_GET['profile_username'] = $_SESSION['user'];
$myProfile = true;

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_block.php';?>
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_menu.php';?>

    </div>

    <div class="main">
        <div class="main_column column">
            <!-- main_column -->
            <h2>I hate posts</h2>
            <div class="hated_posts_area">
                <?php $user->renderHatedPosts();?>
            </div>
            <div id="go_back">UP</div>
        </div>
    </div>

</div>

<script>
$(".post_comments_controls_input").on('input', function (e) {
    // If value len in comment body input is > 15 enable button
    var comment_length = $(e.target).val();
    if (comment_length.length > 15) {
        $('[name="send_comment"]').prop('disabled', false);
    } else {
        $('[name="send_comment"]').prop('disabled', true);
    }
});
</script>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/footer.php';?>