<?php

require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';
$visitedProfile = $user;

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_block.php';?>
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_menu.php';?>

    </div>

    <div class="main">
        <div class="main_column column">
            <!-- main_column -->
            <h2>Admin Page</h2>
        </div>
    </div>

</div>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/footer.php';?>