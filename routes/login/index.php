<?php

$error_message = "";

require $_SERVER['DOCUMENT_ROOT'] . "/includes/register/login_handler.php";
require $_SERVER['DOCUMENT_ROOT'] . "/includes/register/register_handler.php";

// Если авторизован, то сразу на профиль
if (isset($_SESSION["user"])) {
    header("Location: /index.php");
}

$db = get_my_db();

?>

<!DOCTYPE html>
<html lang="en">

<?php require $_SERVER['DOCUMENT_ROOT'] . "/templates/head.php"; ?>

<body>
    <div class="wrapper">
        <h1 class="header">FckNet.</h1>
        <?php if (isset($_GET['register'])): ?>
            <?php require $_SERVER['DOCUMENT_ROOT'] . "/templates/registration_form.php";?>
        <?php else: ?>
            <?php require $_SERVER['DOCUMENT_ROOT'] . "/templates/login_form.php";?>
        <?php endif;?>
    </div>

<script>
    function onSubmitLogin(token) {
        document.getElementById("login_form").submit();
    };

    function onSubmitRegister(token) {
        document.getElementById("register_form").submit();
    };
</script>

</body>
</html>