<?php

require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';
$visitedProfile = $user;

if (isset($_FILES) && isset($_POST["upload"])) {

    $fileinfo = @getimagesize($_FILES["file-input"]["tmp_name"]);
    $width = $fileinfo[0];
    $height = $fileinfo[1];

    $allowed_image_extension = array(
        "png",
        "jpg",
        "jpeg",
        "gif",
    );

    // Get image file extension
    $file_extension = pathinfo($_FILES["file-input"]["name"], PATHINFO_EXTENSION);

    if (!file_exists($_FILES["file-input"]["tmp_name"])) {
        $response = array(
            "type" => "error",
            "message" => "Choose image file to upload.",
        );
    } else if (($_FILES["file-input"]["size"] > (4000000))) {
        $response = array(
            "type" => "error",
            "message" => "Image size exceeds 4Mb",
        );
    } else if (!in_array($file_extension, $allowed_image_extension)) {
        $response = array(
            "type" => "error",
            "message" => "Upload valiid images.",
        );
    } else {
        $username = $_SESSION['user'];
        $path = "assets/images/profile_pic/" . $username . "_" . uniqid() . ".png";
        $target = $_SERVER['DOCUMENT_ROOT'] . "/" . $path;

        cropImage($_FILES["file-input"]["tmp_name"], $file_extension);

        if (move_uploaded_file($_FILES["file-input"]["tmp_name"], $target)) {
            $response = array(
                "type" => "success",
                "message" => "Image uploaded successfully.",
            );
            $db->query("UPDATE users SET profile_pic = ? WHERE username = ?", $path, $username);
            header("Location: index.php");
        } else {
            $response = array(
                "type" => "error",
                "message" => "Problem in uploading image files.",
            );
        }
    }
    unset($_FILES);
}

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_block.php';?>
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_menu.php';?>

    </div>

    <div class="main">
        <div class="main_column column">
            <!-- main_column -->
            <h2>Settings</h2>

            <div class="settings-section">
                <h3>User picture</h3>
                <form action="index.php" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                <div class="custom-file">
                    <input type="file" name="file-input" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                </div>
                <div>
                    <!-- <input class="btn btn-primary" type="submit" name="upload"> -->
                    <button type="submit" name="upload" class="btn btn-primary">Submit</button>
                </div>
                </form>
                <?php if (!empty($response)) {?>
                    <div class="response <?php echo $response["type"]; ?>
                        ">
                        <?php echo $response["message"]; ?>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>

</div>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/footer.php';?>