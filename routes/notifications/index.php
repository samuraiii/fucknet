<?php

include $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';
$visitedProfile = $user;
$notifications = $user->getNotificationsList();

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_block.php';?>
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_menu.php';?>

    </div>

    <div class="main">
        <div class="main_column column">
            <!-- main_column -->
            <h2>Notifications</h2>
            <div class="notifications">
                <?php if (count($notifications) == 0): ?>
                    <div style="color: lightgray; text-align: center;">You have no new notifications!</div>
                <?php else: ?>
                    <?php foreach ($notifications as $notification): ?>
                        <?php require $_SERVER['DOCUMENT_ROOT'] . '/templates/blocks/notification.php';?>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/footer.php';?>