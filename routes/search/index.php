<?php

require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/header.php';
$visitedProfile = $user;

?>

<div class="wrapper">

    <div class="left_column column">

        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_block.php';?>
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/user_menu.php';?>

    </div>

    <div class="main">
        <div class="main_column column">
            <h2>Users</h2>
            <div class="input-group mb-3">
                <input id="search" type="text" class="form-control" minlength="2" placeholder="find...">
                <div class="input-group-append">
                    <button id="find" class="btn btn-outline-secondary" type="button">Find</button>
                </div>
            </div>
            <div class="search-results">
            </div>
        </div><!-- main_column -->

    </div>

</div>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/includes/blocks/footer.php';?>