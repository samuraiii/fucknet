<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>No such page!</title>
</head>

<style>
    body {
        background-color: black;
    }
    h1 {
        color: white;
    }
</style>

<body>
    <div class="wrapper">
        <h1>Nothing is here for you!</h1>
        <a href="/">Home</a>
    </div>
</body>

</html>