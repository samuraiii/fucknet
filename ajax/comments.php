<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header('Method Not Allowed', true, 405);
    exit;
}

require $_SERVER['DOCUMENT_ROOT'] . "/includes/db.php";
require $_SERVER['DOCUMENT_ROOT'] . "/includes/autoload.php";

$db = get_my_db();

if ($_SESSION['user'] == $_REQUEST['userLoggedIn']) {
    if (isset($_REQUEST['get_comments_for'])) {
        $post = new PostItem($db, $_REQUEST['get_comments_for'], new User($db, $_SESSION['user']));
        $post->renderComments();
    } else if (isset($_REQUEST['comment_body'])) {
        $comment_body = htmlspecialchars($_REQUEST['comment_body']);
        if (strlen(trim($comment_body)) < 15) {
            echo "too short";
        } else {
            $post = new PostItem($db, $_REQUEST['post_id'], new User($db, $_SESSION['user']));
            $post->addComment($comment_body);
        }
    }
}
