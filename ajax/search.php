<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header('Method Not Allowed', true, 405);
    exit;
}

require $_SERVER['DOCUMENT_ROOT'] . "/includes/db.php";
require $_SERVER['DOCUMENT_ROOT'] . "/includes/autoload.php";

$db = get_my_db();

$search_request = $_REQUEST['search_request'];
$query = "SELECT username, profile_pic FROM users WHERE username LIKE ?";
$results = $db->query($query, '%' . $search_request . '%')->fetchAll();

$res = "";

if (count($results) == 0) {
    echo "<span style='color: white; font-size: 20px;'>Не нашлось</span>";
}

foreach ($results as $result) {
    $result = new User($db, $result['username']);
    require $_SERVER['DOCUMENT_ROOT'] . '/templates/blocks/search_result.php';
}

echo $res;
