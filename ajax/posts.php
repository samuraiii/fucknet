<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header('Method Not Allowed', true, 405);
    exit;
}

require $_SERVER['DOCUMENT_ROOT'] . "/includes/db.php";
require $_SERVER['DOCUMENT_ROOT'] . "/includes/autoload.php";

$db = get_my_db();
$limit = 10; // Number of posts per call

if ($_SESSION['user'] == $_REQUEST['userLoggedIn']) {

    if ($_REQUEST['action'] == 'load_posts') {
        // Load posts on the wall
        if ($_SESSION['user'] == $_REQUEST['userVisied']) {
            // User loads own wall
            $user = new User($db, $_SESSION['user']);
            $user_wall = new Wall($db, $user);
            $user_wall->loadPosts($_REQUEST, $limit);
        } else if ($_SESSION['user'] != $_REQUEST['userVisied']) {
            // User loads another wall
            $user = new User($db, $_REQUEST['userVisied']);
            $user_wall = new Wall($db, $user);
            $user_wall->loadPosts($_REQUEST, $limit);
        }

    } else if ($_REQUEST['action'] == 'submit_post') {
        // Submit of the post
        $user = new User($db, $_SESSION['user']);
        $visitedProfile = new User($db, $_REQUEST['userVisied']);
        $user_wall = new Wall($db, $visitedProfile);
        $res_id = $user_wall->submitPost($_POST['post_text'], new User($db, $_SESSION['user']));

        if ($res_id == "to_short_post") {
            header('Content-type: application/json');
            echo json_encode(array('status' => $res_id));
        } else {
            header('Content-type: text/html; charset=UTF-8');
            $post = new PostItem($db, $res_id, new User($db, $_SESSION['user']));
            $post->renderHtml();
        }
    }
}
