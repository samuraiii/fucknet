<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header('Method Not Allowed', true, 405);
    exit;
}

require $_SERVER['DOCUMENT_ROOT'] . "/includes/db.php";
require $_SERVER['DOCUMENT_ROOT'] . "/includes/autoload.php";

$db = get_my_db();

if ($_SESSION['user'] == $_REQUEST['userLoggedIn']) {
    if (isset($_REQUEST['post_id'])) {
        $user = new User($db, $_SESSION['user']);
        $post = new PostItem($db, $_REQUEST['post_id'], $user);
        $post->hateToogle($user);
    } else if ($_SESSION['user'] == $_REQUEST['user_hater']) {
        $user_hater = new User($db, $_SESSION['user']);
        $user_hater->hate($_REQUEST['user_to_hate']);
    }
}
