<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header('Method Not Allowed', true, 405);
    exit;
}

require $_SERVER['DOCUMENT_ROOT'] . "/includes/db.php";
require $_SERVER['DOCUMENT_ROOT'] . "/includes/autoload.php";

$db = get_my_db();

if ($_SESSION['user'] == $_REQUEST['user']) {
    if (isset($_REQUEST['id'])) {
        $notification_id = $_REQUEST['id'];
        $user = new User($db, $_SESSION['user']);
        $user->readNotification($notification_id);
    }
}
